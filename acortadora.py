#!/usr/bin/python3

"""
Minipráctica 1 de Antonio Perona Martínez
Ingeniería Telemática
"""

import webapp
import urllib.parse


class Acortadora(webapp.webApp):
    urls = {
    }

    def parse(self, peticion):

        # de la peticion, sacamos su método, su recurso y su cuerpo
        metodo = peticion.split(' ', 2)[0]
        recurso = peticion.split(' ', 2)[1]
        cuerpo = peticion.split('\n')[-1]
        return metodo, recurso, cuerpo

    def process(self, peticion):

        # sacamos método, recurso y cuerpo de la petición
        metodo, recurso, cuerpo = peticion

        form = """
            <form action="" method="POST">
                    <p>Introduce la url original: <input type="text" name="url_original"/>
                    <p>Introduce la url acortada: <input type="text" name="url_acortada"/>
                    <p><input type="submit" value="acortar"/>
            </form>
        """
        codigoError = "404 Not Found"
        cuerpoError = "<html><body>Recurso no encontrado, pulse aqui para volver -> " \
                    '<a href="/">Volver</a> </body></html>'

        if metodo == "GET":
            if recurso in self.urls.keys():  # si la url esta en el diccionario
                httpCode = "200 OK"
                htmlBody = "<html><body>Redirigiendo a: " \
                           + '<a href="' + self.urls[recurso] + ' ">"' + self.urls[recurso] + '"</a><br>'""

            elif recurso == "/":
                httpCode = "200 OK"
                htmlBody = "<html><body>Acortadora de URLs" \
                           "<p>Formulario para acortar urls:</p>" + form \
                           + "<p>Urls acortadas: </p>" + str(self.urls) + "</body></html>"

            else:
                httpCode = codigoError
                htmlBody = cuerpoError

        if metodo == "POST":
            url = urllib.parse.unquote(cuerpo.split("&")[0].split("=")[1])
            acortada = '/' + cuerpo.split('&')[1].split('=')[1]

            if url != "" and acortada != "/":
                self.urls[acortada] = url
                httpCode = "200 OK"
                htmlBody = "<html><body>URL original: " \
                           + '<a href="' + self.urls[acortada] + ' ">"' + self.urls[acortada] + '"</a><br>' \
                           + "<p>URL acortada: "'<a href="' + self.urls[acortada] + ' ">"' + acortada + '"</a></p> </body></html>'

            else:
                httpCode = codigoError
                htmlBody = cuerpoError

        return httpCode, htmlBody


if __name__ == "__main__":
    try:
        AcortadoraApp = Acortadora("localhost", 1234)
    except KeyboardInterrupt:
        print("Closing...")